# GoogleCamera
PRODUCT_PACKAGES += \
    GoogleCamera

PRODUCT_COPY_FILES += \
    packages/apps/GoogleCamera/product/etc/permissions/org.codeaurora.snapcam.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.codeaurora.snapcam.xml
